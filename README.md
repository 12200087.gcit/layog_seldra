# Project title
 Layog Seldra

# Description
Layog Seldra is a web application that connects jobs and job seekers in a single platform. Admin can create new, delete and update vacancies on our site, including complete announcements with job descriptions, and company information. On the other hand, job seekers can easily access a wide range of job opportunities. Job seekers can quickly apply for specific positions directly through our website if they are qualified or interested. We want to make connecting companies and job seekers easier and faster through Layog Seldra, resulting in a more productive and successful hiring process for both parties.


# Problem statement
1. The youth in Bhutan facing high unemployment rates.

2. One contributing factor is the absence of a dedicated platform to search for job vacancies.
    This leads to challenges for job seekers :

3. Limited visibility for job postings.
4. Increased costs for job seekers.
5. Time-consuming  in job searching process, especially for distant locations.

# Aim
    To develop a website that will make job seekers aware of job vacancies and  connect job seekers with employers in a specific industry or location.

# Goal
    The goal of this project is to help the job seeker get information on job vacancies and to help job providers to get an employee that is capable for that particular post.

# Objectives
1. To make aware of job vacancies
2. To connect the job seekers to the employer faster and efficiently.
3. To help employer connecting with qualified employees
4. To save human resources and costs
5. To save time in recruiting and finding jobs

# Scope of the Project
    Scope of the project is limited to job-providers/organizations within Bhutan

# Functionalities
1. Registration
2. Login
3. View Job Vacancy for job Seeker
4. Post Job Vacancy for admin
5. Update job vacancy
6. Delete job vacancy
7. Manage feedback
8. Manage User
9. Apply job for job seeker
10. Search for both admin and job seeker
11. Feedback submission for job seeker
12. Feedback manage for admin
13. Logout for both admin and job seeker