import React, { useState } from 'react';

function SubmissionDetail() {

  const buttonStyles = {
    width: '100px',
    height: '40px',
    backgroundColor: '#450868',
    border: 'none',
    borderRadius: '30px',
  };
  
  const applya={
    color: '#fff',
    fontSize: '16px',
    textDecoration: 'none'
  }
  
  const space4 ={
    fontSize:'24px'
  }
  const container={
    height:'100%',
    width:'100%',
    display: 'flex',
    flexDirection: 'column', 
    alignItems: 'center',
    justifyContent:'center'
  }
  const content={
    flex:'1',
    width:'70%',
    border: '1px solid #ccc',
    padding:'50px',
    marginTop:'18px',
    boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'
  
  }


  return (
    
    <div style={container}>
      <div style={content}>
      <h3 style={{textAlign:'center', paddingBottom:'20px'}}>Vacancy Announcement Details</h3>
      <p ><b>Job Title :</b> Jigme Dorji Wangchuck National Referral  Hospital Vacancy</p>
      <p><b>Organization :</b> Jigme Dorji Wangchuck National Referral  Hospital</p>
      <p> <b>Placement :</b> To be placed at emergency Medicine</p>
      <p><b>Placement :</b> Placement :10/24/2023</p>
      <p><b>Placement :</b> Placement :15/04/2023</p>
      <p><b>Number of vacancy :</b> 04</p>
      <p><b>Position Level :</b> General Duty Medical Officer</p>
      <p><b>Employment type :</b> Contact</p>
      <p><b>Pay Scale :</b> 20,645-26,870</p>
      <p><b>Application Form link :</b><a style={{fontSize:18, paddingLeft:'5px'}} href="https://docs.google.com/forms/d/e/1FAIpQLSd3Fkg9CXu29GYsMNZ5phlRhQvq4Pxh35ohEvUArZexy3rYEA/viewform?usp=sf_link">click here</a></p>
      <p><b>Document requirement :</b> Class X Mark sheet, Class XII Mark sheet</p>
      <p><b>Other details :</b> Min Qualification-MBBS</p>
      <p style={space4}> Contact Information:</p>
      <p><b>Name :</b> Pema Yangki</p>
      <p><b>Email :</b> yangkipema2020@gmail.com</p>
      <p><b>Contact Number :</b> +975 17608043</p>
      <p><b>Office Telephone :</b> 440033</p>
      <button style={buttonStyles}><a style={applya} href="https://docs.google.com/forms/d/e/1FAIpQLSd3Fkg9CXu29GYsMNZ5phlRhQvq4Pxh35ohEvUArZexy3rYEA/viewform?usp=sf_link">Delete</a></button>
      
      </div>
  </div>
  );
}

export default SubmissionDetail;
