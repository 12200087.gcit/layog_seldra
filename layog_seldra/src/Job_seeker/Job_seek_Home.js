import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import HeroSlider from '../components/HeroSlider';
import ButtonMenu from '../components/moreBut/Button';
import Get_Function from '../components/Get_function';
import Job_Seek_NavBar from './Job_Seek_NavBar';
import Footer from '../components/Footer/Footer';
import { useMatch } from 'react-router-dom';
import Search from '../components/Search';

export default function Job_Seek_Home() {

  const match = useMatch('/seek/:cid');
  const cid  = match?.params.cid;
  console.log("Home page cid", cid)

  return (
    <div>


      <Job_Seek_NavBar/>
      <HeroSlider/>
      {/* <p>CID: {cid}</p> */}
      <Search/>

      {/* <Get_Function></Get_Function> */}
        
      {/* <div style={{paddingLeft:'50px',}}>
        <ButtonMenu></ButtonMenu>
      </div> */}

      <Footer/>
    </div>
  )
}
