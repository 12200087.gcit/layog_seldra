import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import banner from '../images/banner.jpg'
import Job_Seek_NavBar from './Job_Seek_NavBar';
import Footer from '../components/Footer/Footer'

export default function All_Job_List() {
  return (
    <div>
        {/* <Job_Seek_NavBar></Job_Seek_NavBar> */}
        <div id="carouselExampleCaptions" className="carousel slide mt-2">
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img src={banner} style={{height:'20rem'}} className="d-block w-100" alt="..."/>
            <div className="carousel-caption mb-5">
              <h2>View all the Job Vacany here</h2>
              <p>Some representative placeholder content for the first slide.</p>
            </div>
          </div>
          
        </div>
      </div>

      <div className="row g-4 px-5 my-4" >
            <div className="col-md-6">
              <div className="card" style={{boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'}}>
                <div className="card-body">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                  <a href="#" className="btn btn-primary">View more</a>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="card" style={{boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'}}>
                <div className="card-body">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                  <a href="#" className="btn btn-primary">View more</a>
                </div>
              </div>
            </div>
        </div>
        <div className="row g-4 px-5 my-4" >
            <div className="col-md-6">
              <div className="card" style={{boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'}}>
                <div className="card-body">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                  <a href="#" className="btn btn-primary">View more</a>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="card" style={{boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'}}>
                <div className="card-body">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                  <a href="#" className="btn btn-primary">View more</a>
                </div>
              </div>
            </div>
        </div>
        <div className="row g-4 px-5 my-4" >
            <div className="col-md-6">
              <div className="card" style={{boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'}}>
                <div className="card-body">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                  <a href="#" className="btn btn-primary">View more</a>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="card" style={{boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'}}>
                <div className="card-body">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                  <a href="#" className="btn btn-primary">View more</a>
                </div>
              </div>
            </div>
        </div>
        <div className="row g-4 px-5 my-4" >
            <div className="col-md-6">
              <div className="card" style={{boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'}}>
                <div className="card-body">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                  <a href="#" className="btn btn-primary">View more</a>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="card" style={{boxShadow:'0px 4px 4px rgba(0, 0, 255, 0.25)'}}>
                <div className="card-body">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                  <a href="#" className="btn btn-primary">View more</a>
                </div>
              </div>
            </div>
        </div>
      <Footer></Footer>
    </div>
  )
}
