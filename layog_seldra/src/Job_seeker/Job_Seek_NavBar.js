import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav, Form, FormControl, Button, Dropdown } from 'react-bootstrap';
import { NavLink ,useNavigate } from 'react-router-dom';
import bb from '../images/ls.jpeg';
import pp from '../images/pp.png';
// import { useNavigate } from 'react-router-dom';




export default function Job_Seek_NavBar() {

  const navigate = useNavigate();

  const handleLogout = () => {
    const confirmLogout = window.confirm("Are you sure you want to logout?");
    if (confirmLogout) {
      navigate('/')
    }
  };
  return (
    <div className='container-fluid px-5 shadow' >
      <Navbar expand="lg">
        <Navbar.Brand href="#"><img style={{ height: '4rem', width: '4rem' }} src={bb} alt="logo" /></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto d-flex gap-md-5">
            <NavLink exact to="/seek" activeClassName="active" className="nav-link">Home</NavLink>
            {/* <NavLink exact to="/myapplied" activeClassName="active" className="nav-link">My Submission</NavLink> */}
            <NavLink exact to="/aboutseek" activeClassName="active" className="nav-link">About</NavLink>
            <NavLink exact to="/contactseek" activeClassName="active" className="nav-link mr-3">Contact</NavLink>
          </Nav>
          {/* <Form inline className='d-flex ms-auto'>
            <FormControl type="search" placeholder="Search" className="mr-sm-2 mx-2" />
            <Button variant="outline-primary">Search</Button>
          </Form> */}

          <Dropdown className='px-4' align="end">
            <Dropdown.Toggle variant="link" id="profile-dropdown">
              <img style={{ height: '2rem', width: '2rem' }} src={pp} alt="logo" />
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {/* <Dropdown.Item href="#">Profile</Dropdown.Item> */}
              {/* <Dropdown.Item href="#">Settings</Dropdown.Item> */}
              <Dropdown.Divider />
              <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>

        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}

// import React, { useState } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import { Navbar, Nav, Form, FormControl, Button, Container, Row, Col } from 'react-bootstrap';
// import bb from '../images/ls.jpeg';
// import pp from '../images/pp.png';

// export default function JobSeekNavBar() {
//   const [showSidebar, setShowSidebar] = useState(false);

//   const toggleSidebar = () => {
//     setShowSidebar(!showSidebar);
//   };

//   return (
//     <>
//       <Navbar expand="lg">
//         <Container fluid>
//           <Navbar.Brand href="#">
//             <img style={{ height: '4rem', width: '4rem' }} src={bb} alt="logo" />
//           </Navbar.Brand>
//           <Navbar.Toggle onClick={toggleSidebar} aria-controls="basic-navbar-nav" />
//           <Navbar.Collapse id="basic-navbar-nav">
//             <Nav className="ms-auto d-flex gap-md-5">
//               <Nav.Link href="#">Home</Nav.Link>
//               <Nav.Link href="/myapplied">My Submission</Nav.Link>
//               <Nav.Link href="/about">About</Nav.Link>
//               <Nav.Link href="/contact">Contact</Nav.Link>
//             </Nav>
//             <Form inline className="d-flex ms-auto">
//               <FormControl type="search" placeholder="Search" className="mr-sm-2 mx-2" />
//               <Button variant="outline-primary">Search</Button>
//             </Form>
//             <Navbar.Brand className="ms-4" href="#" onClick={toggleSidebar}>
//               <img style={{ height: '2rem', width: '2rem' }} src={pp} alt="logo" />
//             </Navbar.Brand>
//           </Navbar.Collapse>
//         </Container>
//       </Navbar>
//       <div className={`sidebar ${showSidebar ? 'active' : ''}`}>
//         <Container fluid>
//           <Row>
//             <Col className="pt-3 pb-2">
//               <h5>Menu</h5>
//             </Col>
//           </Row>
//           <Row>
//             <Col>
//               <Nav className="flex-column">
//                 <Nav.Link href="#">Home</Nav.Link>
//                 <Nav.Link href="/myapplied">My Submission</Nav.Link>
//                 <Nav.Link href="/about">About</Nav.Link>
//                 <Nav.Link href="/contact">Contact</Nav.Link>
//               </Nav>
//             </Col>
//           </Row>
//         </Container>
//       </div>
//     </>
//   );
// }
