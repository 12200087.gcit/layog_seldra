import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import HeroSlider from '../components/HeroSlider';
// import CardComponent from '../components/cardComponent';
import ButtonMenu from '../components/moreBut/Button'
import Get_Function_Home from '../components/Get_for_Home';
import NavBar from './NavBar';
import Footer from '../components/Footer/Footer';

export default function Home() {
  return (
    <div>

      <NavBar/>
      
       <HeroSlider/>

      <Get_Function_Home></Get_Function_Home>
  
      {/* <div>
        <ButtonMenu></ButtonMenu>
      </div> */}
      
      <Footer/>
    </div>
  )
}
