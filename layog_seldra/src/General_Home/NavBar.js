import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav, Form, FormControl, Button, NavDropdown } from 'react-bootstrap';
import { NavLink, useNavigate} from 'react-router-dom';
import bb from '../images/ls.jpeg'

export default function NavBar() {
  const linkStyle = {
    textDecoration: 'none',
    color:'rgba(0, 0, 0, 0.695)',

  };

  const navigate = useNavigate();
  const handleLogout = () => {
      navigate('/login')

  };

  const handleLogout1 = () => {
      navigate('/addminlogin')

  };

  
  return (
    
    <div className='container-fluid px-5 shadow' >
        <Navbar expand="lg">
        <Navbar.Brand  href="#"><img style={{height:'4rem', width:'4rem'}} src={bb} alt="logo"/></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto d-flex gap-md-5">
                <NavLink exact to="/" activeClassName="active" className="nav-link px-2">Home</NavLink>
                <NavLink exact to="/about" activeClassName="active" className="nav-link px-2">About Us</NavLink>
                <NavLink exact to="/contact" activeClassName="active" className="nav-link px-2">Contact Us</NavLink>
              </Nav>
              {/* <Form inline className='d-flex ms-auto'>
                <FormControl type="search" placeholder="Search" className="mr-sm-2 mx-2" />
                <Button variant="outline-primary">Search</Button>
              </Form> */}
              <Nav>
                <NavLink variant="outline-primary" className="mr-sm-2 mx-4 mt-2" exact to="/register" style={linkStyle}>Register</NavLink>
                {/* <NavLink exact to="/login" style={linkStyle}>Login</NavLink> */}
                <NavDropdown className='px-3' title="Login" id="login-dropdown">
                  <NavDropdown.Item onClick={handleLogout1}>Admin</NavDropdown.Item>
                  <NavDropdown.Item onClick={handleLogout}>User</NavDropdown.Item>
              </NavDropdown>
              </Nav>
            </Navbar.Collapse>
      </Navbar>
    </div>
  )
}
