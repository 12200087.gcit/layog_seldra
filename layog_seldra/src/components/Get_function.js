import React,{useState, useEffect, Fragment} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
// import CardComponent from './cardComponent';
import VacancyDetail from '../Job_seeker/VacancyDetail';

export default function Get_Function() {

    const cardStyle = {
        boxShadow: '0px 4px 4px rgba(0, 0, 255, 0.25)',
        height: 'auto',
      };

  const [jobs, setJobs] = useState([]);
//   const [selectedJobId, setSelectedJobId] = useState(null);
//   const [showDetails, setShowDetails] = useState(false);

//   const handleJobDetails = (jobId) => {
//     setSelectedJobId(jobId);
//     setShowDetails(true);
//   };

  useEffect(() => {
    getJobs();
  }, []);

//   console.log(jobs)

  const getJobs = async () => {
    try {
      const response = await fetch('http://localhost:5000/jobs');
      const jsonData = await response.json();
      setJobs(jsonData);
    // console.log(jsonData)
    } catch (err) {
      console.error(err.message);
    }
  };
  const dateFormatter = (d) => {
    const date = new Date(d);
    const day = date.getDate();
    const month = date.getMonth() + 1; // Months are zero-based, so we add 1
    const year = date.getFullYear();
    return `${month}/${day}/${year}`;
  };
  
  

  return (
      <Fragment>
        <div className="row g-4 px-5 my-4">
            {jobs.map((todo) => (
            <div className="col-md-4">

                <div className="card" style={cardStyle}>             
                    <div className="card-body" key={todo.job_id}>
                    
                        <h6 className="text-capitalize"><b>Job title :</b> {todo.jobtitle}</h6>
                        <p className="card-text ms-0 m-1"><b>Organization :</b> {todo.organization}</p>
                        <p className="card-text  ms-0 m-1"><b>Start date :</b> {dateFormatter(todo.sta_date)}</p>
                        <p className="card-text ms-0 m-1"><b>End date :</b> {dateFormatter(todo.end_date)}</p>
                        <p className="card-text ms-0 m-1"><b>Position :</b> {todo.position}</p>
                        
                        <VacancyDetail todo={todo}/>

                    </div>
                </div>
                </div>
            ))}
        </div>

    </Fragment>
  )
}

