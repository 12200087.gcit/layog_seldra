import React,{useState, useEffect, Fragment} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {toast, ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import Emplo_NavBar from '../../Employee/Emplo_NavBar';
import Footer from '../Footer/Footer';

export default function Get_Feedback() {

  const [feedbacks, setFeedbacks] = useState([]);

  useEffect(() => {
    getFeedbacks();
  }, []);

//   console.log(jobs)

  const getFeedbacks = async () => {
    try {
      const response = await fetch('http://localhost:5000/feedbacks');
      const jsonData = await response.json();
      setFeedbacks(jsonData);
    // console.log(jsonData)
    } catch (err) {
      console.error(err.message);
    }
  };
  

  const deleteFeedback = async (id) => {
    try {
      // Prompt the user for confirmation
      const confirmed = window.confirm('Are you sure you want to delete this feedback?');
  
      if (confirmed) {
        // If the user confirms, proceed with the deletion
        const response = await fetch(`http://localhost:5000/feedbacks/${id}`, {
          method: 'DELETE'
        });
        // console.log(response);
  
        // Remove the deleted item from the state
        setFeedbacks((prevTodos) => prevTodos.filter((fd) => fd.feedback_id !== id));
        toast.success('Feedback deleted successfully.');

      // Reload the page after a short delay
      setTimeout(() => {
        // window.location.reload();
      }, 1000);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
      <Fragment>
        <Emplo_NavBar/>
        <ToastContainer/>  
    
        <div class="container mt-3">
            <h3>Feedback Table</h3>        
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>User name</th>
                    <th>Email</th>
                    <th>Message</th>
                </tr>
                </thead>
                <tbody>
                {feedbacks.map((fd) => (
                    <tr key={fd.feedback_id}>

                        <td> {fd.user_name}</td>
                        <td>{fd.email}</td>
                        <td>{fd.feedback_message}</td>
                        <td class="d-flex justify-content-center">
                            <button
                                className="btn btn-danger"
                                onClick={() => deleteFeedback(fd.feedback_id)}
                                >
                                Delete
                            </button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
            </div>
        <Footer/>

    </Fragment>
  )
}

