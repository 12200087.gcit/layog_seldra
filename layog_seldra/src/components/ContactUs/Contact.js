import React,{useState} from 'react'
import styled from "styled-components";
import banner from '../../images/banner.jpg'
import './Contact.css'
import HeroBanner from '../HeroEmp';
import {toast, ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import Footer from '../Footer/Footer';
import NavBar from '../../General_Home/NavBar';
import { useNavigate } from 'react-router-dom';

const Contact = () => {
    const navigate = useNavigate();

    const handlefeed = () => {
      
        navigate('/login')
       
    };

    const [user_name, setUser_name] = useState('');
    const [email, setEmail] = useState('');
    const [feedback_message, setFeedback_message] = useState('');

    const onSubmitFeedback = async (e) => {
        e.preventDefault();
    
        try {
          // if (!title || !placement || !numberOfEmployees || !startDate) {
          //   // Perform form validation
          //   setValidationError('Please fill in all fields.');
          //   return;
          // }
    
          const body = {
           user_name,
           email,
           feedback_message

          };
    
          const response = await fetch('http://localhost:5000/feedbacks', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body)
          });
    
          if (response.ok) {
            toast.success('Successful.')
            setTimeout(()=>{
              // window.location.reload('/createjob')
            }, 1000)
            setUser_name('');
            setEmail('');
            setFeedback_message('');
           
          } else {
            // Handle error responses
            const errorData = await response.json();
            throw new Error(errorData.message);
          }
    
          // window.location = "/";
          
        } catch (err) {
          console.error(err.message);
          // Display error message to the user
          alert('An error occurred. Please try again.');
        }
      };

  return (  
    <div>     
        <ToastContainer/>  
         <NavBar/>
        <HeroBanner titleProp={"Contact Us"} content={"Hi, For further queries and feedback, Please contact us"} imageSrc={banner} />
        
        <div className="m-5">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3547.3691914679825!2d91.19056951109268!3d27.238948176355933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x375eaa156ca2a677%3A0x281e6e8f6a9cf6ea!2sGyalpozhing%20College%20of%20Information%20Technology!5e0!3m2!1sen!2sbt!4v1683645577147!5m2!1sen!2sbt" 
            width="100%" 
            height="450" 
            style={{border:0}} 
            allowFullScreen="" 
            loading="lazy" 
            referrerpolicy="no-referrer-when-downgrade">

            </iframe>
            <p style={{fontFamily:'Verdana Pro', fontWeight:'bold'}}>Contact Us for More Information And Convinience</p>
        </div>

        <section className="m-5 p-3" style={{borderRadius:'5px', border:'1px solid gray'}}>
        <h3 className="h1-responsive font-weight-bold text-center my-1">Feedback</h3>       
            <p className="text-center w-responsive mx-auto mb-5">Your feedbacks are valuable for us and your future.</p>
            <div className="row">
        
                <div className="col-md-12 mb-md-0 mb-5">
                    <form id="contact-form" name="contact-form" action="mail.php" method="POST" >
                        <div className="row">
                            <div className="col-md-2"></div>

                            <div className="col-md-4">
                                <div className="md-form mb-0">
                                    <label for="name" className="">Your name</label>
                                    <input type="text" id="name" name="name" class="form-control" placeholder='name' value={user_name}
                                    onChange={(event) => setUser_name(event.target.value)}
                                    required/>
                                
                                </div>
                            </div>
                
                            <div className="col-md-4">
                                <div className="md-form mb-0">
                                    <label for="email" className="">Your email</label>
                                    <input type="text" id="email" name="email" className="form-control" value={email}
                                    onChange={(event) => setEmail(event.target.value)}
                                    required/>
                                    
                                </div>
                            </div>
                            <div className="col-md-2"></div>
                        </div>
            
                        <div className="row mt-4">
                            <div className="col-md-2"></div>
                            <div className="col-md-8">
                                <div className="md-form mb-0">
                                    <label for="subject" className="">Message her</label>
                                    <input type="text" id="subject" name="subject" className="form-control" value={feedback_message}
                                    onChange={(event) => setFeedback_message(event.target.value)}
                                    required/>
                                    
                                </div>
                            </div>
                            <div className="col-md-2"></div>
                        </div>

                        <div className="text-center text-md-left">
                        <button onClick={handlefeed} type='button' style={{padding: '10px 60px', backgroundColor: '#6067AD',color: 'white',border: 'none', borderRadius: '25px',fontSize: '1.2rem',cursor: 'pointer',marginTop:'3%'}}>Submit</button>
                    </div>

                    </form>

                    
                </div>
            </div>
        </section>

        <Footer/>
 
    </div>
  )
}

export default Contact