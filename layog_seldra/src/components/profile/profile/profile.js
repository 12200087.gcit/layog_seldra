
import React, { useState, Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import Cookies from 'js-cookie';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components'
import ls from '../../../images/ls.jpeg'
// import styles from './styles.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { ToastContainer, toast } from 'react-toastify';
import './profile.css'
import Emplo_NavBar from '../../../Employee/Emplo_NavBar';
import Footer from '../../Footer/Footer';


function Profile() {

  return (
    <Fragment>
      <div className="container mt-5">
      <div className="row d-flex justify-content-center"> 
          <div className="col-md-7">
              <div className="card p-3 py-4">  
                  <div className="text-center">
                      <img src="https://i.imgur.com/bDLhJiP.jpg" width="100" className="rounded-circle"/>
                  </div>
                  
                  <div className="text-center mt-3">
                      {/* <span className="bg-secondary p-1 px-4 rounded text-white">Pro</span> */}
                      <h5 className="mt-2 mb-0">Tandin Jamtsho</h5>
                      <span className="mt-2">12200087.gcit@rub.edu.bt</span><br></br>
                      <span className="mt-2">11904000111</span><br></br>
                      <span className="mt-2 ">174656454</span><br></br>
                      
                      <ul className="social-list">
                          <li><i className="fa fa-facebook"></i></li>
                          <li><i className="fa fa-dribbble"></i></li>
                          <li><i className="fa fa-instagram"></i></li>
                          <li><i className="fa fa-linkedin"></i></li>
                          <li><i className="fa fa-google"></i></li>
                      </ul> 
                      <div className="buttons">
                          <button className="btn btn-outline-primary px-4">Edit profile</button>
                          <button className="btn btn-primary px-4 ms-3">Logout</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</Fragment>  

  );
}

export default Profile;
