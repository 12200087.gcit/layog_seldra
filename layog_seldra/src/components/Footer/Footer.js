import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import './Footer.css'
import logo from './ls.jpeg'

export default function Footer() {
  

  return (
   
    <div style={{backgroundColor: '#344374'}}>
      <div className="row g-4 px-5 my-4" >
        <div className="col-md-3">
          
          <div className="card-body" style={{color:'white'}}>
            <img src={logo} className='mx-4' style={{height:'4.5rem', width:'4.5rem' }}/>
            <p className="link-text">Tied & Trusted</p>
          </div>

        </div>
      <div className="col-md-3">
    
        <div className="link-body" style={{color:'white'}}>
          <h5 className="link-title">Quick Links</h5>
          <a href="https://www.moice.gov.bt" className="link-text">Ministry of Industry, Commerce & Employment</a><br/>
          <a href="https://jobs.rcsc.gov.bt" className="link-text">Zhiyog Recruitment System</a><br/>
        </div>
      </div>
      <div className="col-md-3" >
        
        <div className="link-body" style={{color:'white'}}>
          <h5 className="link-title">Service</h5>
          <a href="https://docs.google.com/document/d/13uoJPxkQUmGiCB8TsrnNsQ4kLeWGfJB3-1uK_NZR52Q/edit?usp=sharing" className="link-text">Term and conditions</a><br/>
          <a href="https://docs.google.com/document/d/1lsC93ae_2bLmqPYt7Ixu6GUxzg-9Ehm-6hyn5cGbvII/edit?usp=sharing" className="link-text">Download documents</a><br/>
        </div>
        
      </div>
      <div className="col-md-3">
        
        <div className="card-body" style={{color:'white'}}>
          <h5 className="card-title">Connect to us</h5>
        
          <div className='social-icons'>
            <a href="https://www.facebook.com" target="_blank" rel="noopener noreferrer" className="icon">
              <FontAwesomeIcon icon={faFacebook} />
            </a>
            <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer" className="icon">
              <FontAwesomeIcon icon={faTwitter} />
            </a>
            <a href="https://www.instagram.com" target="_blank" rel="noopener noreferrer" className="icon">
              <FontAwesomeIcon icon={faInstagram} />
            </a>
          </div>
        </div>
        
      </div>
      </div> 

      {/* copy right */}
      <div className="mt-3 w-100 text-center" style={{color:'white'}} >
        <hr />
          <p style={{fontSize:"0.75rem"}} className="mx-auto">Copyright ©2023 | All Right Reserved</p>
        <br/>
      </div>

    </div>
  )
}
