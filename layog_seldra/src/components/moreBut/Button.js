import React from 'react'
import './Button.css'

const ButtonMenu = () => {
  return (
    <div className="center">
      <button className="view-more-button">View More</button>
    </div>
  )
}

export default ButtonMenu