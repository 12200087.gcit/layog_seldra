import React from 'react';

const CardComponent = ({ cardTitle, cardText, cardText1,cardText2, cardText3 }) => {
  const cardStyle = {
    boxShadow: '0px 4px 4px rgba(0, 0, 255, 0.25)',
    height: 'auto',
  };

  return (
    <div className="card" style={cardStyle}>
      <div className="card-body">
        <h6 className="text-capitalize"><b>Job title :</b> {cardTitle}</h6>
        <p className="card-text ms-0 m-1"><b>Organization</b> {cardText}</p>
        <p className="card-text  ms-0 m-1"><b>Start date :</b> {cardText1}</p>
        <p className="card-text ms-0 m-1"><b>End date :</b> {cardText2}</p>
        <p className="card-text ms-0 m-1"><b>Position :</b> {cardText3}</p>
        {/* <a href="/vacancydetail" className="btn btn-primary mt-2">View details</a> */}
      </div>
    </div>
  );
};
  

export default CardComponent;