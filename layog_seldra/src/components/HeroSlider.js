import React from 'react';
import hero from '../images/herobanner.png'
// import hero1 from '../images/'
import hero2 from '../images/herobanner2.jpeg'
const HeroSlider = () => {
  return (
    <div id="carouselExampleCaptions" className="carousel slide mt-2">
      <div className="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>
      <div className="carousel-inner">
        <div className="carousel-item active">
          <img src={hero2} className="d-block w-100" alt="..." />
          <div className="carousel-caption d-none d-md-block">
            <h4>Best Way To Get Your Job.</h4>
            <p>Let us be your trusted partner in the job search process, offering resources, personalized assistance along the way.</p>
          </div>
        </div>
        <div className="carousel-item">
          <img src={hero} className="d-block w-100" alt="..." />
          <div className="carousel-caption d-none d-md-block">
            <h5>Best Way To Get Your Job.</h5>
            <p>We are here to assist you in finding your dream job and navigating the path to success.</p>
          </div>
        </div>
        <div className="carousel-item">
          <img src={hero2} className="d-block w-100" alt="..." />
          <div className="carousel-caption d-none d-md-block">
            <h5>Best Way To Get Your Job.</h5>
            <p>Finding a job can be challenging, but with our guidance and expertise, we are here to make the process easier for you.</p>
          </div>
        </div>
      </div>
      <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  );
};

export default HeroSlider;
