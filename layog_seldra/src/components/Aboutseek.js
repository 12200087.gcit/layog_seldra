import React from 'react';
import styled from 'styled-components';
import { Container, Row, Col } from 'react-bootstrap';
import myImage from "../images/banner.jpg";
import 'bootstrap/dist/css/bootstrap.min.css';
import Slider from 'react-slick';
import banner from '../images/banner.jpg'
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import HeroBanner from './HeroEmp';
import Job_Seek_NavBar from '../Job_seeker/Job_Seek_NavBar';
import Footer from './Footer/Footer';
// import v1 from "../assets/p.mp4";
// import v2 from "../assets/r.mp4";
// import v3 from "../assets/vi.mp4";


const BoxTitle = styled.p`
  font-size: 24px;
  transition: all 0.3s ease-in-out;
`;

const BoxText = styled.p`
  font-size: 16px;
  transition: all 0.3s ease-in-out;
`;

const BoxContainer = styled.div`
  width: 90%;
  height: 150px;
  background-color: #D9D9D9;
  align-items: center;
  justify-content: center;
  margin: 8% auto;
  border-radius:7px;
  padding:2%;
  transition: all 0.8s ease-in-out;
  
  &:hover {
    background-color: #6067AD;
    transform: scale(1.1);
  }
  &:hover ${BoxTitle} {
    color: #fff;
    font-size: 26px;
  }
  &:hover ${BoxText} {
    color: #fff;
    font-size: 18pxpx;
  }
`;

const VideoSliderContainer = styled.div`
  position: absolute;
  margin-top:3%;
  margin-left: 12%;
  width: 65vh;
  height: 50vh;
  z-index: 100;
  background-color: #fff;
  overflow-x: auto;
  -webkit-overflow-scrolling: touch;
  scroll-snap-type: x mandatory;
  scrollbar-width: none;
  -ms-overflow-style: none;

  &::-webkit-scrollbar {
    display: none;
  }

  & > * {
    scroll-snap-align: center;
  }
  
`;

const VideoSliderTitle = styled.h2`
  font-size: 24px;
  margin-bottom: 10px;
`;

const TextContainer = styled.div`
  display:flex;
  flex-direction:column;
  justify-content:flex-start;
  width: 40%;
  height: 50%;
  margin-left:55%;
  margin-top:5%;

  
`;

const AboutTitle = styled.h2`
  color: black;
  font-size: 24px;
`;

const Text = styled.p`
  color: black;
  font-size: 18px;
`;

const ThreeBoxContainer = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-wrap: wrap;
  margin: 0 auto;
  width: 85%;
  margin-top:7%;
  

  @media (max-width: 768px) {
    flex-direction: column;
    align-items: stretch;
  }
`;

const Box = styled.div`
  width: 25%;
  height: 150px;
  background-color: #6067AD;
  border: 1px solid #ccc;
  border-radius:10px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 3%;
  
  transition: all 0.6s ease-in-out;
  
  &:hover {
    background-color: #6067AD;
    transform: scale(1.1);
  }

  @media (max-width: 768px) {
    width: 100%;
    height: 150px;
  }
`;

const ThreeBoxText = styled.p`
  font-size: 18px;
  text-align: center;
  color: black;
`;

// const ThreeBoxTitle = styled.h2`
//   color: black;
//   font-size: 24px;
// `;
const AboutUsSeek = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
      };

    return (
      <>
        <div>
        <Job_Seek_NavBar/>
        <HeroBanner titleProp={"About Us"} content={"Hi, Know more about Layog Seldra"} imageSrc={banner} />
        </div>

        <Container>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <BoxContainer>
                            <BoxTitle>Mission</BoxTitle>
                            <BoxText>We provide platform for both employers and job seekers to find job easily and efficiently</BoxText>
                        </BoxContainer>
                    </div>
                    <div className="col">
                        <BoxContainer>
                            <BoxTitle>Vision</BoxTitle>
                            <BoxText>To help job seekers to find job easily and employers to meet with quality candidate.</BoxText>
                        </BoxContainer>
                    </div>
                </div>
            </div>
        </Container>

   
    <VideoSliderContainer>
      <VideoSliderTitle>Video</VideoSliderTitle>
      <Slider {...settings}>
        <div >
          <video 
            width="100%" 
            height="315"
            controls>
            <source type='video/mp4'/>
           </video>
        </div>
        <div>
          <video 
            width="100%" 
            height="315"
            controls>
            <source type='video/mp4'/>
          </video>
        </div>
        <div>
          <video 
           width="100%" 
           height="315" 
           controls>
             <sourc type='video/mp4'/>
           </video>
        </div>
      </Slider>
    </VideoSliderContainer>
    
   <Container>
      <Row>
        <Col>
          <TextContainer>
            <AboutTitle>About Us</AboutTitle>
            <Text>Layog Seldra is a web application that connects jobs and job seekers in a single platform. 
              Job seekers can easily access a wide range of job opportunities. Job seekers can quickly apply 
              for specific positions directly through our website if they are qualified or interested. We want 
              to make connecting companies and job seekers easier and faster through Layog Seldra, resulting 
              in a more productive and successful hiring process for both parties.</Text>
            </TextContainer>
            </Col>
          </Row>
      </Container> 
     
      
      <ThreeBoxContainer>
      <Box>
        <ThreeBoxText>Phone : +975 77464522</ThreeBoxText>
      </Box>
      <Box>
        <ThreeBoxText>Email : layogseldra@gmail.com.</ThreeBoxText>
      </Box>
      <Box>
        <ThreeBoxText>Website : https//:www.layogseldra.com.</ThreeBoxText>
      </Box>
    </ThreeBoxContainer>

    <Footer/>
    
    </>
    
    );
  };
export default AboutUsSeek;