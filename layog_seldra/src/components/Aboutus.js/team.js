// import React from 'react';
// import "./team.css";
// import logo1 from "../../images/pp.png";
// import logo2 from "../../images/pp.png";
// import logo3 from "../../images/pp.png";
// import logo4 from "../../images/pp.png";

// import { BsWhatsapp, BsFacebook, BsInstagram } from 'react-icons/bs';
// import m1 from "../../images/pp.png";
// import m2 from "../../images/pp.png";
// import m3 from "../../images/pp.png";
// import m4 from "../../images/pp.png";

// function TeamSection() {
//   return (
//     <section className="section-team">
//       <div className="container">

//         <div className="row justify-content-center text-center">
//           <div className="col-md-8 col-lg-6">
//             <div className="header-section">
//               <h3 className="small-title">Our Experts</h3>
//               <h2 className="title">Let's meet with our team members</h2>
//             </div>
//           </div>
//         </div>

//         <div className="row">
//           <div className="col-sm-6 col-lg-4 col-xl-3">
//             <div className="single-person">
//               <div className="person-image">
//                 <img src={m1} alt="" />
//                 <span className="icon">
//                 <img src={logo1} className='logo' alt="" />
//                 </span>
//               </div>

//               <div className="person-info">
//                 <h3 className="full-name">Tandin Wangchuk</h3>
//                 <span className="speciality">Web Developer</span>
//               </div>

//               <div className="connect">
//                   <a href="https://www.youtube.com/">
//                         <BsFacebook className="cLogo"/>
//                     </a>
//                     <a href="https://www.youtube.com/" className="iLink">
//                         <BsInstagram  className="cLogo" />
//                     </a>
//                     <a href="https://www.youtube.com/" className="wLink">
//                         <BsWhatsapp  className="cLogo" />
//                     </a>
//               </div>  

//             </div>
//           </div>

//           <div className="col-sm-6 col-lg-4 col-xl-3">
//             <div className="single-person">
//               <div className="person-image">
//                 <img src={m3} alt="" />
//                 <span className="icon">
//                 <img src={logo2} className='logo' alt="" />
//                 </span>
//               </div>
//               <div className="person-info">
//                 <h3 className="full-name">Pema Lhamo</h3>
//                 <span className="speciality">WordPress Developer</span>
//               </div>
//               <div className="connect">
//                   <a href="https://www.youtube.com/">
//                         <BsFacebook className="cLogo"/>
//                     </a>
//                     <a href="https://www.youtube.com/" className="iLink">
//                         <BsInstagram  className="cLogo" />
//                     </a>
//                     <a href="https://www.youtube.com/" className="wLink">
//                         <BsWhatsapp  className="cLogo" />
//                     </a>
//               </div>  

//             </div>
//           </div>

//           <div className="col-sm-6 col-lg-4 col-xl-3">
//             <div className="single-person">
//               <div className="person-image">
//                 <img src={m2} alt="" />
//                 <span className="icon">
//                 <img src={logo3} className='logo' alt="" />
//                 </span>
//               </div>
//               <div className="person-info">
//                 <h3 className="full-name">Sonam Deki</h3>
//                 <span className="speciality">Angular Developer</span>
//               </div>
//               <div className="connect">
//                   <a href="https://www.youtube.com/">
//                         <BsFacebook className="cLogo"/>
//                     </a>
//                     <a href="https://www.youtube.com/" className="iLink">
//                         <BsInstagram  className="cLogo" />
//                     </a>
//                     <a href="https://www.youtube.com/" className="wLink">
//                         <BsWhatsapp  className="cLogo" />
//                     </a>
//               </div>  

//             </div>
//           </div>

//           <div className="col-sm-6 col-lg-4 col-xl-3">
//             <div className="single-person">
//               <div className="person-image">
//                 <img src={m4} alt="" />
//                 <span className="icon">
//                 <img src={logo4} className='logo' alt="" />
//                 </span>
//               </div>
//               <div className="person-info">
//                 <h3 className="full-name">Yuadhistra Hang Subba</h3>
//                 <span className="speciality">Javascript Developer</span>
//               </div>
//               <div className="connect">
//                   <a href="https://www.youtube.com/">
//                         <BsFacebook className="cLogo"/>
//                     </a>
//                     <a href="https://www.youtube.com/" className="iLink">
//                         <BsInstagram  className="cLogo" />
//                     </a>
//                     <a href="https://www.youtube.com/" className="wLink">
//                         <BsWhatsapp  className="cLogo" />
//                     </a>
//               </div>  

//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// }

// export default TeamSection;