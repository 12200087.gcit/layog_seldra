import React from 'react';

const HeroBanner = ({ titleProp, content, imageSrc }) => {
    return (
      <div id="carouselExampleCaptions" className="carousel slide mt-2">
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img src={imageSrc} style={{ height: '20rem' }} className="d-block w-100" alt="..." />
            <div className="carousel-caption mb-5">
              <h2>{titleProp}</h2>
              <p>{content}</p>
            </div>
          </div>
        </div>
      </div>
    );
  };

export default HeroBanner;