import React,{useState, useEffect, Fragment} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {FormControl, Form} from 'react-bootstrap';
import VacancyDetail from '../Job_seeker/VacancyDetail';

export default function Search() {

  const cardStyle = {
    boxShadow: '0px 4px 4px rgba(0, 0, 255, 0.25)',
    height: 'auto',
  };



  const [products, setProducts] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredProducts, setFilteredProducts] = useState([]);
  // const [newFurnitureAdded, setNewFurnitureAdded] = useState(false);

//   const handleCategorySelect = (event) => {
//     setSelectedCategory(event.target.value);
//   }

//   const handleSearch = (event) => {
//     setSearchTerm(event.target.value);
//   }

  useEffect(() => {
    const getJobs= async () => {
      try {
        const response = await fetch("http://localhost:5000/jobs");
        const data = await response.json();

        setProducts(data);
      } catch (error) {
        console.error(error);
      }
    };
   
    getJobs();
 
  }, []);



  useEffect(() => {
    const filterProducts = () => {
      let filtered = selectedCategory ? products.filter(product => product.category === selectedCategory) : products;
      
      if (searchTerm) {
        filtered = filtered.filter(product => product.jobtitle.toLowerCase().includes(searchTerm.toLowerCase()));
      }

      setFilteredProducts(filtered);
    };

    filterProducts();

  }, [selectedCategory, products, searchTerm]);

  console.log(filteredProducts);

  const dateFormatter = (d) => {
    const date = new Date(d);
    const day = date.getDate();
    const month = date.getMonth() + 1; // Months are zero-based, so we add 1
    const year = date.getFullYear();
    return `${month}/${day}/${year}`;
  };
  
  return (
    
    <Fragment>
        <Form inline className='d-flex ms-auto p-3'>
            <FormControl type="search" placeholder="Search here" className="mr-sm-2 mx-2" value={searchTerm} onChange={e=>setSearchTerm(e.target.value)}/>
            {/* <Button variant="outline-primary" onClick={handleSearch}>Search</Button> */}
        </Form>
        {/* <input type='search' value={searchTerm} onChange={e=>e.target.value}></input>
        <Button onClick={handleSearch}></Button> */}
        <div className="row g-4 px-5 my-4">
            {filteredProducts.map((todo) => (
            <div className="col-md-4">

                <div className="card" style={cardStyle}>             
                    <div className="card-body" key={todo.job_id}>
                    
                        <h6 className="text-capitalize"><b>Job title :</b> {todo.jobtitle}</h6>
                        <p className="card-text ms-0 m-1"><b>Organization :</b> {todo.organization}</p>
                        <p className="card-text  ms-0 m-1"><b>Start date :</b> {dateFormatter(todo.sta_date)}</p>
                        <p className="card-text ms-0 m-1"><b>End date :</b> {dateFormatter(todo.end_date)}</p>
                        <p className="card-text ms-0 m-1"><b>Position :</b> {todo.position}</p>
                        
                        <VacancyDetail todo={todo}/>

                    </div>
                </div>
                </div>
            ))}
        </div>

    </Fragment>
  )
}
