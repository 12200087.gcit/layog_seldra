import React, {useState} from 'react';
import { BrowserRouter, Router, Routes, Route} from 'react-router-dom';
import Home from "../src/General_Home/Home"
import Job_Seek_Home from './Job_seeker/Job_seek_Home';
import Emplo_Home from './Employee/Emplo_Home';
import AboutUs from './components/AbooutUs';
import VacancyDetail from './Job_seeker/VacancyDetail';
import My_Applied from "../src/Job_seeker/My_Applied"
import CreateJob from './Employee/Create_job';
import Contact from './components/ContactUs/Contact';
import ViewDetail_emp from './Employee/ViewDetails_emp';
import ForgotPassword from './Registration_Form/ForgotPassword';
import Profile from './components/profile/profile/profile';
import Get_Feedback from './components/ContactUs/get_feedback';
import RegistrationForm from './Login_Form/Signup';
import LoginForm from './Login_Form/Login';
import AboutUsSeek from './components/Aboutseek';
import Contactseek from './components/ContactUs/ContactusSeek';
import Get_Users from './Employee/Get_users';
// import Registration from './Registration_Form/AdminReg';
import LoginFormAdmin from './Login_Form/AdminLogin';
import About from './components/Aboutus.js/About';

function App() {
  // const [isAuthenticated, setIsAuthenticated] = useState(true);
  // const [userRole, setUserRole] = useState('');

  return (
    <BrowserRouter>

      {/* {isAuthenticated && userRole === 'employer' ? (
            <Provider/>
          ) : isAuthenticated && userRole === 'jobseeker' ? (
            <Seeker />
          ) : (
            <NavBar/>
          )} */}
          <div>
            
          <Routes> 
          <Route exact path={'/'} element={<Home/>}/>
          <Route path={'/about'} element={<AboutUs/>}/>
          <Route path="/contact" element={<Contact/>}/>
          <Route path={'/aboutseek'} element={<AboutUsSeek/>}/>
          <Route path="/contactseek" element={<Contactseek/>}/>

          <Route path="/register" element={<RegistrationForm/>}/>
            <Route path="/login" element={<LoginForm/>}/> 
            <Route path="/forgotpassword" element={<ForgotPassword/>}/>

          <Route exact path={'/seek'} element={<Job_Seek_Home/>}/>
          <Route path="/myapplied" element={<My_Applied/>}/>
          <Route path="/vacancydetail" element={<VacancyDetail/>}/>

          <Route exact path={'/emp'} element={<Emplo_Home/>}/>
          <Route path="/createjob" element={<CreateJob/>}/> 
          <Route path="/viewupdate" element={<ViewDetail_emp/>}/>
          <Route path="/feedback" element={<Get_Feedback/>}/>
          <Route path="/users" element={<Get_Users/>}/>
          <Route path="/profile" element={<Profile/>}/>
          {/* <Route path="/admin" element={<Registration/>}/> */}
          <Route path="/addminlogin" element={<LoginFormAdmin/>}/>
          



        
        </Routes>
        </div>

      
      {/* <Footer/> */}

    </BrowserRouter>
  );
}
// function Seeker() {
//   return (
//     <div>
//       <Job_Seek_NavBar></Job_Seek_NavBar>
//       <Routes> 

//         <Route exact path={'/seek'} element={<Job_Seek_Home/>}/>
//         <Route path="/myapplied" element={<My_Applied/>}/>
//         <Route path="/vacancydetail" element={<VacancyDetail/>}/> 

//       </Routes>

//     </div>

//   );
// }
// function Provider() {
//   return (
//     <div>
//       <Emplo_NavBar></Emplo_NavBar>
//       <Routes>      
//         <Route exact path={'/emp'} element={<Emplo_Home/>}/>
//         <Route path="/createjob" element={<CreateJob/>}/> 
//         <Route path="/viewupdate" element={<ViewDetail_emp/>}/>
//         <Route path="/feedback" element={<Get_Feedback/>}/>

//       </Routes>

//     </div>

//   );
// }

export default App;










