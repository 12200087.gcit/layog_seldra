import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav, Form, FormControl, Button, Dropdown } from 'react-bootstrap';
import { NavLink ,useNavigate} from 'react-router-dom';
import bb from '../images/ls.jpeg';
import pp from '../images/pp.png';

export default function Emplo_NavBar() {

  const navigate = useNavigate();

  const handleLogout = () => {
    const confirmLogout = window.confirm("Are you sure you want to logout?");
    if (confirmLogout) {
      navigate('/')
      // Perform your logout logic here
      // Clear any user-related data or tokens from storage
      // Redirect the user to the login page or any other desired page
    }
  };
  return (
    <div className='container-fluid px-5 shadow'>
      <Navbar expand="lg">
        <Navbar.Brand href="#"><img style={{ height: '4rem', width: '4rem' }} src={bb} alt="logo" /></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto d-flex gap-md-5">
            <NavLink exact to="/emp" activeClassName="active-link" className="nav-link">Home</NavLink>
            <NavLink exact to="/createjob" activeClassName="active-link" className="nav-link">Create Job</NavLink>
            <NavLink exact to="/feedback" activeClassName="active-link" className="nav-link">Feedback</NavLink>
            <NavLink exact to="/users" activeClassName="active-link" className="nav-link">Manage user</NavLink>
          </Nav>
          {/* <Form inline className='d-flex ms-auto'>
            <FormControl type="search" placeholder="Search" className="mr-sm-2 mx-2" />
            <Button variant="outline-primary">Search</Button>
          </Form> */}
          <Dropdown className='px-4' align="end">
            <Dropdown.Toggle variant="link" id="profile-dropdown">
              <img style={{ height: '2rem', width: '2rem' }} src={pp} alt="logo" />
            </Dropdown.Toggle>
            <Dropdown.Menu>
            {/* <Dropdown.Item as={NavLink} to="/profile">Profile</Dropdown.Item> */}
              {/* <Dropdown.Item href="#">Settings</Dropdown.Item> */}
              <Dropdown.Divider />
              <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}
