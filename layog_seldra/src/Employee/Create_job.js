import React, { useState, Fragment } from 'react';
import styled from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'react-bootstrap';
import {toast, ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import Emplo_NavBar from './Emplo_NavBar';
import Footer from '../components/Footer/Footer';

const FormContainer = styled.div`
  max-width: 70%;
  margin: 0 auto;
  margin-top: 2%;
  padding: 5%;
  border: 1px solid black;
  border-radius: 5px;
`;

const FormHeader = styled.h1`
  font-size: 2rem;
  margin-bottom: 20px;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;
const Button = styled.button`
  padding: 10px 60px;
  background-color: #6067AD;
  color: white;
  border: none;
  border-radius: 25px;
  font-size: 1.2rem;
  cursor: pointer;
  margin-top: 3%;
`;

const CreateJob = () => {
  const [jobtitle, setJobTitle] = useState('');
  const [organization, setOrganization] = useState('');
  const [sta_date, setStart] = useState(new Date().toISOString().slice(0, 10));
  const [end_date, setEnd] = useState(new Date().toISOString().slice(0, 10));
  const [position, setPosition] = useState('');
  const [emp_number, setNumber] = useState(0);
  const [employmenttype, setEmployementtype] = useState('');
  const [pay, setPay] = useState(0);
  const [link, setLink] = useState('');
  const [reqdoc, setReqdoc] = useState('');
  const [person_name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [contact, setContact] = useState('');
  const [tele, setTele] = useState('');
  

  function handleStartDateChange(event) {
    setStart(event.target.value);
  }

  function handleDateChange(event) {
    setEnd(event.target.value);
  }

  function handleChange(event) {
    const inputMobileNumber = event.target.value;
    const formattedMobileNumber = inputMobileNumber.replace(/\D/g, '');
    setContact(formattedMobileNumber);
  }

  function handleteleChange(event) {
    const inputMobileNumber = event.target.value;
    const formattedMobileNumber = inputMobileNumber.replace(/\D/g, '');
    setTele(formattedMobileNumber);
  }

  const onSubmit = async (e) => {
    e.preventDefault();

    try {
      // if (!title || !placement || !numberOfEmployees || !startDate) {
      //   // Perform form validation
      //   setValidationError('Please fill in all fields.');
      //   return;
      // }

      const body = {
        jobtitle, 
        organization, 
        sta_date, 
        end_date, 
        position, 
        emp_number, 
        employmenttype, 
        pay, 
        link, 
        reqdoc, 
        person_name, 
        email, 
        contact, 
        tele
      };

      const response = await fetch('http://localhost:5000/jobs', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body)
      });

      if (response.ok) {
        toast.success('Successful.')
        setTimeout(()=>{
          // window.location.reload('/createjob')
        }, 1000)
        setJobTitle('');
        setOrganization('');
        setStart('');
        setEnd('');
        setPosition('');
        setNumber('');
        setEmployementtype("");
        setPay('');
        setLink('');
        setReqdoc('');
        setName('');
        setEmail('');
        setContact('');
        setTele('');
        // setValidationError('');
      } else {
        // Handle error responses
        const errorData = await response.json();
        throw new Error(errorData.message);
      }

      // window.location = "/";
      
    } catch (err) {
      console.error(err.message);
      // Display error message to the user
      alert('An error occurred. Please try again.');
    }
  };

  return (
    <Fragment>
      <Emplo_NavBar/>
      <FormContainer>
        <ToastContainer/>
        
        <FormHeader>Create new new job vacancy</FormHeader>
        <form onSubmit={onSubmit}>
          <div className="form-floating mb-3">
            <input
              type="text"
              className="form-control"
              id="title"
              name="title"
              placeholder="Job Title"
              value={jobtitle}
              onChange={(event) => setJobTitle(event.target.value)}
              required
            />
            <label htmlFor="title">Job Title:</label>
          </div>

          <div className="form-floating mb-3">
            <input
              type="text"
              className="form-control"
              id="org"
              name="org"
              placeholder="Organization"
              value={organization}
              onChange={(event) => setOrganization(event.target.value)}
              required
            />
            <label htmlFor="org">Organization:</label>
          </div>

          <Container>
            <Row className="justify-content-md-center">
              <Col md={6}>
                <div className="form-floating mb-3">
                  <input
                    type="date"
                    className="form-control"
                    id="start"
                    name="start"
                    placeholder="Starting date"
                    value={sta_date}
                    onChange={handleStartDateChange}
                    required
                  />
                  <label htmlFor="start">Starting date: {sta_date}</label>
                </div>
              </Col>
              <Col md={6}>
                <div className="form-floating mb-3">
                  <input
                    type="date"
                    className="form-control"
                    id="end"
                    name="end"
                    placeholder="End date"
                    value={end_date}
                    onChange={handleDateChange}
                    required
                  />
                  <label htmlFor="end">End date: {end_date}</label>
                </div>
              </Col>
            </Row>

            <Row className="justify-content-md-center">
              <Col md={6}>
                <div className="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="position"
                    name="position"
                    placeholder="Position Level"
                    value={position}
                    onChange={(event) => setPosition(event.target.value)}
                    required
                  />
                  <label htmlFor="position">Position Level:</label>
                </div>
              </Col>
              <Col md={6}>
                <div className="form-floating mb-3">
                  <input
                    type="number"
                    className="form-control"
                    id="num"
                    name="num"
                    placeholder="Number of vacancy"
                    value={emp_number}
                    onChange={(event) => setNumber(Number(event.target.value))}
                    required
                  />
                  <label htmlFor="num">Number of vacancy:</label>
                </div>
              </Col>
            </Row>

            <Row className="justify-content-md-center">
            <Col md={6}>
                <div className="form-floating mb-3">
                  <select
                    className="form-select"
                    id="employmenttype"
                    name="employmenttype"
                    value={employmenttype}
                    onChange={(event) => setEmployementtype(event.target.value)}
                    required
                  >
                    <option value="">Select Employment Type</option>
                    <option value="Full-time">Full-time</option>
                    <option value="Part-time">Part-time</option>
                    <option value="Contract">Contract</option>
                    <option value="Freelance">Freelance</option>
                    <option value="Internship">Internship</option>
                  </select>
                  {/* <label htmlFor="employmenttype">Employment Type:</label> */}
                </div>
              </Col>
              <Col md={6}>
                <div className="form-floating mb-3">
                  <input
                    className="form-control"
                    type="number"
                    id="pay"
                    name="pay"
                    placeholder="Basic payment"
                    value={pay}
                    onChange={(event) => setPay(event.target.value)}
                    required
                  />
                  <label htmlFor="pay">Basic Pay:</label>
                </div>
              </Col>
            </Row>
            <Row className="justify-content-md-center">
              <Col md={6}>
                <div className="form-floating mb-3">
                  <input
                    className="form-control"
                    id="form-link"
                    name="form-link"
                    placeholder="Application form link"
                    value={link}
                    onChange={(event) => setLink(event.target.value)}
                    required
                  />
                  <label htmlFor="form-link">Application form link:</label>
                </div>
              </Col>
              <Col md={6}>
                <div className="form-floating mb-3">
                  <input
                    className="form-control"
                    id="requirements"
                    name="requirements"
                    placeholder="Document requirements"
                    value={reqdoc}
                    onChange={(event) => setReqdoc(event.target.value)}
                    required
                  />
                  <label htmlFor="requirements">Document requirements:</label>
                </div>
              </Col>
            </Row>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                placeholder="Your name"
                value={person_name}
                onChange={(event) => setName(event.target.value)}
                required
              />
              <label htmlFor="name">Your name:</label>
            </div>

            <div className="form-floating mb-3">
              <input
                type="email"
                className="form-control"
                id="email"
                name="email"
                placeholder="Your email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
              <label htmlFor="email">Your email:</label>
            </div>

            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="mobile"
                name="mobile"
                placeholder="Your mobile number"
                value={contact}
                maxLength={8}
                onChange={handleChange}
                required
              />
              <label htmlFor="mobile">Your mobile number:</label>
            </div>

            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="tele"
                name="tele"
                placeholder="Your telephone number"
                value={tele}
                onChange={handleteleChange}
                required
              />
              <label htmlFor="tele">Your telephone number:</label>
            </div>

            <ButtonContainer>
            <Button type="submit">Submit</Button>
          </ButtonContainer>
          </Container>
        </form>
        
      </FormContainer>
    <Footer/>
    </Fragment>
  );
};

export default CreateJob;
