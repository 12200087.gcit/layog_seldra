import React, { Fragment } from 'react';
import { Link, Route, Routes } from 'react-router-dom';

function ViewDetail_emp({ todo }) {

  // console.log("laso" ,todo)

  const dateFormatter = (d) => {
    const date = new Date(d);
    const day = date.getDate();
    const month = date.getMonth() + 1; // Months are zero-based, so we add 1
    const year = date.getFullYear();
    return `${month}/${day}/${year}`;
  };
  
  return (

    <Fragment>
      {/* <div class="container mt-3">  */}
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target={`#id${todo.job_id}`}>
          View more
        </button>

        {/* </div> */}

        <div class="modal fade" id={`id${todo.job_id}`}>
          <div class="modal-dialog">
            <div class="modal-content">

          
              <div class="modal-header">
                <h5 class="modal-title">Details</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
              </div>

             
              <div class="modal-body">
              <div class="container mt-3">
                {/* <h2>Basic List Group</h2> */}
                <ul class="list-group">
                  <li class="list-group-item"><b>Job Title :</b> {todo.jobtitle}</li>
                  <li class="list-group-item"><b>Organization :</b> {todo.organization}</li>
                  <li class="list-group-item"><b>Start date :</b> {dateFormatter(todo.sta_date)}</li>
                  <li class="list-group-item"><b>End date :</b> {dateFormatter(todo.end_date)}</li>
                  <li class="list-group-item"><b>Position :</b> {todo.position}</li>
                  <li class="list-group-item"><b>No. of employee required :</b> {todo.emp_number}</li>
                  <li class="list-group-item"><b>Employement type :</b> {todo.employmenttype}</li>
                  <li class="list-group-item"><b>Payment scale :</b> {todo.pay}</li>
                  {/* <li class="list-group-item"><b>Application link :</b> {todo.link}</li> */}
                  <li class="list-group-item"><b>Document Requirement :</b> {todo.reqdoc}</li>  
                </ul>

                <ul style={{ marginTop: '20px' }} class="list-group">
                <h5 class="list-group-item">Contact information </h5>
                  <li class="list-group-item"><b>Name :</b> {todo.person_name}</li>
                  <li class="list-group-item"><b>Contact no. :</b> {todo.contact}</li>
                  <li class="list-group-item"><b>Email :</b> {todo.email}</li>
                  <li class="list-group-item"><b>Telephone no. :</b> {todo.tele}</li>

                </ul>
              </div>

              </div>

            
              <div class="modal-footer">
              {todo.link && (
                
                
                <a href={todo.link} target="_blank" class="btn btn-primary" >
                  Apply
                </a>
                
              )}
              
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
              </div>

            </div>
          </div>
        </div>


    </Fragment>

  );
}

export default ViewDetail_emp;
