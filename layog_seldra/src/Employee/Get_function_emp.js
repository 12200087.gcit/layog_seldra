import React,{useState, useEffect, Fragment} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import VacancyUpdate from './VacancyUpdate';
import { toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {FormControl, Form} from 'react-bootstrap';
// import ViewDetail_emp from './ViewDetails_emp';

export default function Get_Function_emp() {

  const cardStyle = {
    boxShadow: '0px 4px 4px rgba(0, 0, 255, 0.25)',
    height: 'auto',
  };



  const [products, setProducts] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredProducts, setFilteredProducts] = useState([]);
  // const [newFurnitureAdded, setNewFurnitureAdded] = useState(false);

//   const handleCategorySelect = (event) => {
//     setSelectedCategory(event.target.value);
//   }

//   const handleSearch = (event) => {
//     setSearchTerm(event.target.value);
//   }

  useEffect(() => {
    const getJobs= async () => {
      try {
        const response = await fetch("http://localhost:5000/jobs");
        const data = await response.json();

        setProducts(data);
      } catch (error) {
        console.error(error);
      }
    };
   
    getJobs();
 
  }, []);



  useEffect(() => {
    const filterProducts = () => {
      let filtered = selectedCategory ? products.filter(product => product.category === selectedCategory) : products;
      
      if (searchTerm) {
        filtered = filtered.filter(product => product.jobtitle.toLowerCase().includes(searchTerm.toLowerCase()));
      }

      setFilteredProducts(filtered);
    };

    filterProducts();

  }, [selectedCategory, products, searchTerm]);

  console.log(filteredProducts);

  const dateFormatter = (d) => {
    const date = new Date(d);
    const day = date.getDate();
    const month = date.getMonth() + 1; // Months are zero-based, so we add 1
    const year = date.getFullYear();
    return `${month}/${day}/${year}`;
  };
  
  const deleteJob = async (id) => {
    try {
      // Prompt the user for confirmation
      const confirmed = window.confirm('Are you sure you want to delete this item?');
  
      if (confirmed) {
        // If the user confirms, proceed with the deletion
        const response = await fetch(`http://localhost:5000/jobs/${id}`, {
          method: 'DELETE'
        });
        // console.log(response);
  
        // Remove the deleted item from the state
        setProducts((prevTodos) => prevTodos.filter((todo) => todo.job_id !== id));
        toast.success('Job deleted successfully.');

      // Reload the page after a short delay
      setTimeout(() => {
        window.location.reload();
      }, 3000);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
      <Fragment>
        <ToastContainer/>
        <Form inline className='d-flex ms-auto p-3'>
            <FormControl type="search" placeholder="Search here" className="mr-sm-2 mx-2" value={searchTerm} onChange={e=>setSearchTerm(e.target.value)}/>
            {/* <Button variant="outline-primary" onClick={handleSearch}>Search</Button> */}
        </Form>
        <div className="row g-4 px-5 my-4">
            {filteredProducts.map((todo) => (
            <div className="col-md-4">

                <div className="card" style={cardStyle}>             
                    <div className="card-body" key={todo.job_id}>
                    
                        <h6 className="text-capitalize"><b>Job title :</b> {todo.jobtitle}</h6>
                        <p className="card-text ms-0 m-1"><b>Organization :</b> {todo.organization}</p>
                        <p className="card-text  ms-0 m-1"><b>Start date :</b> {dateFormatter(todo.sta_date)}</p>
                        <p className="card-text ms-0 m-1"><b>End date :</b> {dateFormatter(todo.end_date)}</p>
                        <p className="card-text ms-0 m-1"><b>Position :</b> {todo.position}</p>
                        
                        <VacancyUpdate update={todo}/>
                        {/* <ViewDetail_emp todo={todo} /> */}
                        <button
                            className="btn btn-danger"
                            onClick={() => deleteJob(todo.job_id)}
                            >
                            Delete
                        </button>
                    </div>
                </div>
                </div>
            ))}
        </div>

    </Fragment>
  )
}