import React, { useState, Fragment } from 'react';

function VacancyUpdate({ update }) {

    const dateFormatter = (d) => {
        const date = new Date(d);
        const day = date.getDate();
        const month = date.getMonth() + 1; // Months are zero-based, so we add 1
        const year = date.getFullYear();
        return `${month}/${day}/${year}`;
      };
    
    const [jobtitle, setJobTitle] = useState(update.jobtitle);
    const [organization, setOrganization] = useState(update.organization);
    const [sta_date, setStart] = useState(update ? dateFormatter(update.sta_date) : new Date().toISOString().slice(0, 10));
    const [end_date, setEnd] = useState(update ? dateFormatter(update.end_date) : new Date().toISOString().slice(0, 10));
    const [position, setPosition] = useState(update.position);
    const [emp_number, setNumber] = useState(update.emp_number);
    // console.log("laso" ,update.emp_number)
    const [employmenttype, setEmployementtype] = useState(update.employmenttype);
    const [pay, setPay] = useState(update.pay);
    const [link, setLink] = useState(update.link);
    const [reqdoc, setReqdoc] = useState(update.reqdoc);
    const [person_name, setName] = useState(update.person_name);
    const [email, setEmail] = useState(update.email);
    const [contact, setContact] = useState(update.contact);
    const [tele, setTele] = useState(update.tele);

    

    const updateJob = async e=>{
        e.preventDefault();
        try {
            const body = {jobtitle , organization, sta_date, end_date, position, emp_number, employmenttype, pay, link, reqdoc, person_name, email, contact, tele}
            const response = await fetch(`http://localhost:5000/jobs/${update.job_id}`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body)
            });

            // console.log(response);
            window.location='/emp'

        } catch (err) {
            console.log(err.message)
        }

    }

    const handleUpdate = () => {
        setJobTitle(jobtitle);
        setOrganization(organization);
        setStart(sta_date);
        setEnd(end_date);
        setPosition(position);
        setNumber(emp_number);
        setEmployementtype(employmenttype);
        setPay(pay);
        setLink(link);
        setReqdoc(reqdoc);
        setName(person_name);
        setContact(contact);
        setEmail(email);
        setTele(tele);
      };
  
  return (

    <Fragment>
        <button type="button" className="btn btn-primary m-2" data-bs-toggle="modal" data-bs-target={`#id${update.job_id}`}>
          View & Update
        </button>

        <div className="modal fade" id={`id${update.job_id}`} onClick={e=>{handleUpdate(e)}}>
          <div className="modal-dialog">
            <div className="modal-content">

          
              <div className="modal-header">
                <h5 className="modal-title">Details</h5>
                {/* <button type="button" className="btn-close" data-bs-dismiss="modal"></button> */}
              </div>

             
              <div className="modal-body" size="lg">
              <div className="container mt-3">
              <div className="container mt-3">
                {/* <h2>Stacked form</h2> */}
                <form action="/">
                    <div className="mb-3 mt-3">
                        <label htmlFor="jobtitle">Job title :</label>
                        <input type='text'className='form-control' value={jobtitle} 
                        onChange={(e) => setJobTitle(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >Organization :</label>
                        <input type='text'className='form-control' value={organization} 
                        onChange={(e) => setOrganization(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >Start date :</label>
                        <input type='text'className='form-control' value={sta_date} onChange={(e) => setStart(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >End date :</label>
                        <input type='text'className='form-control' value={end_date} onChange={(e) => setEnd(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >Position :</label>
                        <input type='number'className='form-control' value={position} onChange={(e) => setPosition(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >No. of employee require :</label>
                        <input type='number'className='form-control' value={emp_number} onChange={(e) => setNumber(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >Employement type :</label>
                        <select
                            className="form-control"
                            value={employmenttype}
                            onChange={(e) => setEmployementtype(e.target.value)}
                            >
                            <option value="">Select Employment Type</option>
                            <option value="Full-time">Full-time</option>
                            <option value="Part-time">Part-time</option>
                            <option value="Contract">Contract</option>
                            <option value="Freelance">Freelance</option>
                        </select>
                    </div>

                    <div className="mb-3">
                        <label >Payment scale :</label>
                        <input type='number'className='form-control' value={pay} onChange={(e) => setPay(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >Document Requirements :</label>
                        <input type='text'className='form-control' value={reqdoc} onChange={(e) => setReqdoc(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >Name :</label>
                        <input type='text'className='form-control' value={person_name} onChange={(e) => setName(e.target.value)}/>
                    </div>
                    
                    <div className="mb-3">
                        <label >Contact no. :</label>
                        <input type='number' maxLength={8} className='form-control' value={contact} onChange={(e) => setContact(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >Email :</label>
                        <input type='email'className='form-control' value={email} onChange={(e) => setEmail(e.target.value)}/>
                    </div>

                    <div className="mb-3">
                        <label >Telephone no. :</label>
                        <input type='number'className='form-control' value={tele} onChange={(e) => setTele(e.target.value)}/>
                    </div>

                </form>
                </div>
              </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success" data-bs-dismiss="modal" onClick={e=>{updateJob(e)}}>Update</button>
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal" onClick={e=>{handleUpdate(e)}}>Close</button>
              </div>
            </div>
          </div>
        </div>


    </Fragment>

  );
}

export default VacancyUpdate;
