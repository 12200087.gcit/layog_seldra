import React,{useState, useEffect, Fragment} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {toast, ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import Emplo_NavBar from './Emplo_NavBar';
import Footer from '../components/Footer/Footer';

export default function Get_Users() {

  const [users, setusers] = useState([]);

  useEffect(() => {
    getusers();
  }, []);

//   console.log(jobs)

  const getusers = async () => {
    try {
      const response = await fetch('http://localhost:5000/signup');
      const jsonData = await response.json();
      setusers(jsonData);
    // console.log(jsonData)
    } catch (err) {
      console.error(err.message);
    }
  };
  

  const deleteUsers = async (id) => {
    try {
      // Prompt the user for confirmation
      const confirmed = window.confirm('Are you sure you want to delete this User?');
  
      if (confirmed) {
        // If the user confirms, proceed with the deletion
        const response = await fetch(`http://localhost:5000/signup/${id}`, {
          method: 'DELETE'
        });
        // console.log(response);
  
        // Remove the deleted item from the state
        setusers((prevTodos) => prevTodos.filter((u) => u.user_id !== id));
        toast.success('User deleted successfully.');

      // Reload the page after a short delay
      setTimeout(() => {
        // window.location.reload();
      }, 1000);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
      <Fragment>
        <ToastContainer/>  
        <Emplo_NavBar/>
    
        <div class="container mt-3">
            <h3>Manage users</h3>        
            <table class="table table-bordered">
                <thead>
                <tr>
                    {/* <th>User id</th> */}
                    <th>User CID</th>
                    <th>User name</th>
                    <th>Email</th>
                    <th>Phone number</th>
                </tr>
                </thead>
                <tbody>
                {users.map((u) => (
                    <tr key={u.user_id}>
                        {/* <td> {u.user_id}</td> */}
                        <td> {u.cid}</td>
                        <td> {u.first_name}</td>
                        <td>{u.email}</td>
                        <td>{u.phone_number}</td>
                        <td className="d-flex justify-content-center">
                            <button
                                className="btn btn-danger"
                                onClick={() => deleteUsers(u.user_id)}
                                >
                                Delete
                            </button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
            </div>
        <Footer/>

    </Fragment>
  )
}

