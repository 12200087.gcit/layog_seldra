import React,{Fragment} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import HeroBanner from '../components/HeroEmp';
import banner from '../images/banner.jpg'
import ButtonMenu from '../components/moreBut/Button';
import Get_Function_emp from './Get_function_emp';
import Emplo_NavBar from './Emplo_NavBar';
import Footer from '../components/Footer/Footer';


export default function Emplo_Home() {
  return (

    <Fragment>
      <Emplo_NavBar/>
      <HeroBanner titleProp={"My Post"} content={"Hi, Some representative placeholder content for the first slide."} imageSrc={banner} />

        <Get_Function_emp></Get_Function_emp>

        {/* <ButtonMenu></ButtonMenu> */}

        <Footer/>

    </Fragment>
  )
}

