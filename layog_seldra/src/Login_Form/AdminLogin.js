import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import Cookies from 'js-cookie';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components'
import ls from '../images/ls.jpeg'
// import styles from './styles.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { ToastContainer, toast } from 'react-toastify';



function LoginFormAdmin({setIsLoggedIn, setShowLoginForm}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const[errorMessage,setErrorMessage]=useState('')
  const [isLoading, setIsLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [incorrectPasswordError, setIncorrectPasswordError] = useState('');

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const validateEmail = () => {
    if (!email) {
      setEmailError('Email is required');
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      setEmailError('Invalid email address');
    } else {
      setEmailError('');
    }
  };
  
  const validatePassword = () => {
    if (!password) {
      setPasswordError('Password is required');
    } else {
      setPasswordError('');
    }
  };

  // const handleEmailChange = (event) => {
  //   setEmail(event.target.value);
  // };
  
  // const handlePasswordChange = (event) => {
  //   setPassword(event.target.value);
  // };
  
  const navigate = useNavigate();
  
  const handleSubmit = async (event) => {
    event.preventDefault();
    validateEmail();
    validatePassword();
    
    if (!emailError && !passwordError) {
      try {
        setIsLoading(true);
        // used to send a POST request to the endpoint with the email and password values as the request body.
        const response = await axios.post('http://localhost:5000/loginAdmin', {
          email,
          password,
        });
        console.log(response.data); 

        const authToken = response.data.authToken;
        localStorage.setItem('useremail', response.data.email);
        Cookies.set('authToken', authToken);
        localStorage.setItem('authToken', authToken);
        console.log(authToken);
        setIsLoading(true);

       
        toast.success('Login Successful.')
        setTimeout(()=>{
            navigate('/emp');

        },2000);
      

      } catch (error) {
        console.error(error);
        if (error.response && error.response.status === 401) {
          setIncorrectPasswordError('Incorrect email or password');
        } else {
          setErrorMessage('Error occurred while logging in');
        }
      }finally{
        setIsLoading(false)
      }
    }
  };
  
const Button = styled.button`
  outline: 0;
  background:#344374;
  width: 100%;
  border:0;
  border-radius: 3px;
  /* padding: 15px; */
  color: #ffffff;
  font-size: 15px;
  transition: all 0.4s ease-in-out;
  cursor: pointer;

  &:hover,
  &:active,
  &:focus {
    background:#344374;
    font-size:20px
  }
`;

  return (
    <div style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      minHeight: '100vh',
      width: '100%',
    }}>
      <ToastContainer position='top-center' />
      <form style={{
        backgroundColor:'#344374',
        width: '90%',
        maxWidth: '420px',
        height: 'auto',
        background: 'white',
        boxShadow: 'rgba(0, 0, 0, 0.19) 0px 10px 20px, rgba(0, 0, 0, 0.23) 0px 6px 6px',
        padding: '40px',
      }} onSubmit={handleSubmit}>
        
        <div className='cont'>
        <img
          src={ls}
          alt="Image"
          style={{
            maxWidth: '30%',
            height: 'auto',
            display: 'block',
            margin: '0 auto',
          }}
        />
        <h3 style={{textAlign:'center'}}>Login as Admin</h3>
          {/* <br /> */}
          <input
          style={{
          width: '100%',
          padding: '10px',
          fontSize: '18px',
          borderRadius: '4px',
          margin: '15px 0',
          fontFamily: 'Titillium Web, sans-serif',
          paddingRight: '40px', // Add padding to accommodate the icon
        }}
        type="text"
        id="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        onBlur={validateEmail}
        placeholder='email'
        className='input'
      />
          {emailError && <span style={{ color: 'red' }}>{emailError}</span>}
          <br />
          <div className="row" style={{ display: 'flex', alignItems: 'center' }}>
          <div className="col-12 col-md-11" style={{ position: 'relative' }}>
        <input
          style={{
            width: '110%',
            padding: '10px',
            fontSize: '18px',
            borderRadius: '4px',
            margin: '15px 0',
            fontFamily: 'Titillium Web, sans-serif',
            paddingRight: '40px', // Add padding to accommodate the icon
          }}
          type={showPassword ? 'text' : 'password'}
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          onBlur={validatePassword}
          placeholder="Password"
          className="input"
        />
        <div
          className="d-flex align-items-center justify-content-center"
          style={{
          
            height: '50px',
            width: '20%',
            position: 'absolute',
            top: '50%',
            right: '-27px',
            transform: 'translateY(-50%)',
            cursor: 'pointer',
          }}
        >
          <FontAwesomeIcon
            color="black"
            icon={showPassword ? faEyeSlash : faEye}
            onClick={togglePasswordVisibility}
          />
        </div>
      </div>
      </div>
    
          <span style={{ color: 'red' }} className="error">{passwordError}</span>
          {incorrectPasswordError && <span style={{ color: 'red' }}>{incorrectPasswordError}</span>}
          <Button style={{marginTop:'1rem', padding: '15px', width: '100%' }} type="submit">
            {isLoading ? 'Loading...' : 'Log In'}
          </Button>
    
          {/* <p style={{
            color: '#989898',
            fontSize: '16px',
            position: 'relative',
            marginTop: '2rem',
            fontFamily: "Titillium Web, sans-serif",
            fontSize: '18px',
            textAlign: 'center'
          }}>
            If you don't have an account&nbsp;
            <NavLink style={{textDecoration:'none',}} to="/register">
              Register
            </NavLink>
          </p> */}
        </div>
      </form>
    </div>  

  );
}

export default LoginFormAdmin;