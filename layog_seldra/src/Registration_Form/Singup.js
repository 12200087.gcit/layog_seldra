import React, { Fragment, useState } from "react";
import { Link} from "react-router-dom";
import { toast } from "react-toastify";

const Signup = ({ setAuth }) => {
  const [inputs, setInputs] = useState({
    user_cid:"",
    User_name: "",
    user_email: "",
    user_phone:"",
    user_password: "",
    
  });

  const { user_cid, user_name , user_email, user_phone, user_password} = inputs;

  const onChange = e =>
    setInputs({ ...inputs, [e.target.name]: e.target.value });

  const onSubmitForm = async e => {
    e.preventDefault();
    try {
      const body = { user_cid, user_name , user_email, user_phone, user_password };
      const response = await fetch(
        "http://localhost:5000/authentication/register",
        {
          method: "POST",
          headers: {
            "Content-type": "application/json"
          },
          body: JSON.stringify(body)
        }
      );

      const parseRes = await response.json();

      if (parseRes.jwtToken) {
        localStorage.setItem("token", parseRes.jwtToken);
        setAuth(true);
        toast.success("Register Successfully");
      } else {
        setAuth(false);
        toast.error(parseRes);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <Fragment>
      <div  className ="container">
      <h1 className="mt-5 text-center">Register</h1>
      <form onSubmit={onSubmitForm}>

      <input
          type="number"
          name="user_cid"
          value={user_cid}
          placeholder="User CID"
          onChange={e => onChange(e)}
          className="form-control my-3"
        />

        <input
          type="text"
          name="user_name"
          value={user_name}
          placeholder="Username"
          onChange={e => onChange(e)}
          className="form-control my-3"
        />

        <input
          type="text"
          name="user_email"
          value={user_email}
          placeholder="Email"
          onChange={e => onChange(e)}
          className="form-control my-3"
        />

        <input
          type="number"
          name="user_phone"
          value={user_phone}
          placeholder="Phone number"
          onChange={e => onChange(e)}
          className="form-control my-3"
        />

        <input
          type="password"
          name="user_password"
          value={user_password}
          placeholder="Password"
          onChange={e => onChange(e)}
          className="form-control my-3"
        />

       
        <button type="submit" className="btn btn-success btn-block">Submit</button>
      </form>
      <Link to="/login">login</Link>
      </div>
     
    </Fragment>
  );
};

export default Signup;




// import React from 'react'
// import './singup.css'

// function Singup() {
//   return (
//     <div className='container1'>
//         <form class="form_main" action="">
//          <p class="heading">Registration</p>
//          <div class="inputContainer">
//         <svg viewBox="0 0 16 16" fill="#2e2e2e" height="16" width="16" xmlns="http://www.w3.org/2000/svg" class="inputIcon">
//         <path d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z"></path>
//         </svg>
//         <input placeholder="Username" id="username" class="inputField" type="text"/>
//         </div>
//         <div class="inputContainer">
//         <svg viewBox="0 0 16 16" fill="#2e2e2e" height="16" width="16" xmlns="http://www.w3.org/2000/svg" class="inputIcon">
//         <path d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z"></path>
//         </svg>
//         <input placeholder="Email" id="email" class="inputField" type="email"/>
//         </div>
//         <div class="inputContainer">
//         <svg viewBox="0 0 16 16" fill="#2e2e2e" height="16" width="16" xmlns="http://www.w3.org/2000/svg" class="inputIcon">
//         <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"></path>
//         </svg>
//         <input placeholder="Password" id="password" class="inputField" type="password"/>
//         </div>
//         <div class="inputContainer">
//         <svg viewBox="0 0 16 16" fill="#2e2e2e" height="16" width="16" xmlns="http://www.w3.org/2000/svg" class="inputIcon">
//         <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"></path>
//         </svg>
//         <input placeholder="Confirm Password" id="password" class="inputField" type="password"/>
//     </div>
              
           
//     <button id="button">Register</button>
//     {/* <a className='text' href=''>Forgot password</a> */}
//         <div style={{flexDirection:'row'}}>  
//           <p style={{marginLeft:'20px'}}>Already have account? <a href='/login' className='linktext'>Login</a></p>
          
//         </div>
//     </form>
//     </div>
    
//   )
// }

// export default Singup
