import React, { useState } from 'react';
import axios from 'axios';
import styled from 'styled-components'
import ls from '../images/ls.jpeg'
import {toast, ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
// 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

const Registration = () => {



  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  // const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPassword1, setShowPassword1] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const togglePasswordVisibility1 = () => {
    setShowPassword1(!showPassword1);
  };

  const Button = styled.button`
  outline: 0;
  background:#344374;
  width: 100%;
  border:0;
  border-radius: 3px;
  padding: 15px;
  color: #ffffff;
  font-size: 15px;
  transition: all 0.4s ease-in-out;
  cursor: pointer;

  &:hover,
  &:active,
  &:focus {
    background:#344374;
    font-size:20px
    /* color:#F7931E;
    border: 1px solid #F7931E; */
  }
`;

  async function createUser(email,password) {
    setIsLoading(true)
    try {
      const response = await axios.post('http://localhost:5000/admin', {
       
        email, 
        password,
        
      });
      setIsLoading(false)
      console.log(response.data);
      
      // setSuccessMessage('Account created successfully!');
      toast.success('Account successfully created !.')
        setTimeout(()=>{
          window.location.reload();
      }, 3000)
        
    } catch (error) {
      console.error(error);
      setIsLoading(false)
      setErrorMessage('Error creating account!');
    }
  }

  async function handleSubmit (e) {
    e.preventDefault();
  
    const email = document.getElementById('email').value;
  
  
    const password = document.getElementById('password').value;
   

    if (!email || !password) {
      setErrorMessage('Please fill in all required fields.');
      return;
    }
    
    if (password !== confirmPassword) {
      setErrorMessage('Passwords do not match.');
      return;
    }

    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%?&])[A-Za-z\d@$!%?&]{8,}$/;
    if (!passwordRegex.test(password)) {
      setErrorMessage(
        'Password should contain at least 8 characters, including one special character, one lowercase letter, one uppercase letter, and one digit.'
      );
      return;
    }
    
    await createUser(email, password);
  }

  return (
    
    <div style={{display:'flex',alignItems:'center',justifyContent:'center',
      height:'100vh',width:'100%'}}>
        <ToastContainer/>
      <form onSubmit={handleSubmit}
        style={{width:'420px',height:'900px',
        background:'white',
        boxShadow:'rgba(0, 0, 0, 0.19) 0px 10px 20px, rgba(0, 0, 0, 0.23) 0px 6px 6px',
        padding:'40px', marginTop:'80px'}}>

        <div className='contant' style={{marginTop:'30px'}}>
        <h2 style={{textAlign:'center',color:'#989898'}} className='heading'>Create new account</h2>
        
        <img
          src={ls}
          alt="Image"
          style={{
            maxWidth: '30%',
            height: 'auto',
            display: 'block',
            margin: '0 auto',
          }}
        />
       
        </div>
        <div>
          <input 
            style={{width:'100%',padding:'10px',borderRadius:'4px',
            margin:'15px 0',
            fontSize:'18px',
            fontFamily:"Titillium Web, sans-serif"}}
          type="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} placeholder='Email' className='inputfild'/>
        </div>
      
        
  <div class="row" >
      <div class="col-11" >
    
          <input 
          style={{width:'110%',padding:'10px',borderRadius:'4px',
          margin:'15px 0',
          fontSize:'18px',
          fontFamily:"Titillium Web, sans-serif"}}
          type={showPassword ? 'text' : 'password'}id="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder='Password' className='inputfild'/>
      </div>
      <div class="col-1" style={{display:'flex',alignItems:'center'}}>
      <FontAwesomeIcon
            style={{position:'relative',right:'30px'}}
            
            icon={showPassword ? faEyeSlash : faEye}
            onClick={togglePasswordVisibility}
          />
      </div>
    </div>
          
  <div class="row">
      <div class="col-11" >
    
      <input
            style={{width:'110%',padding:'10px',borderRadius:'4px',
            margin:'15px 0',
            fontSize:'18px',

            fontFamily:"Titillium Web, sans-serif"}}
          type={showPassword1 ? 'text' : 'password'} id="confirmPassword" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} placeholder='Confirm Password' className='inputfild'/>
      </div>
      <div class="col-1" style={{display:'flex',alignItems:'center'}}>
      <FontAwesomeIcon
            style={{position:'relative',right:'30px'}}
            icon={showPassword1 ? faEyeSlash : faEye}
            onClick={togglePasswordVisibility1}
          />
      </div>
    </div>
      
        {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
        {/* {successMessage && <p style={{ color: 'green' }}>{successMessage}</p>} */}

        <Button style={{marginTop:'1rem',padding:'15px',width:'100%'}} type="submit">
            {isLoading ?'Loading...':'Register'}</Button>
          {/* <p style={{position:'relative',bottom:'2rem',
        fontSize:'19px',
        color:'#989898'}}>Already a member?<NavLink to='/' className='loginlink'> LogIn</NavLink></p>  */}
      </form>
    </div>
  );
}

export default Registration;