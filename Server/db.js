const Pool = require("pg").Pool;

const pool = new Pool({
    user: "postgres",
    password: "pgadmin123",
    host: "localhost",
    port: 5432,
    database: "Layog_DB"

})

module.exports = pool;
