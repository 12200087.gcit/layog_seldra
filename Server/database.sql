CREATE DATABASE Layog_DB

CREATE TABLE user1(

  user_id SERIAL PRIMARY KEY,
  user_cid VARCHAR(255),
  user_name VARCHAR(255),
  user_email VARCHAR(255),
  phone_number VARCHAR(255)
  hash_password VARCHAR(255), 
);

CREATE TABLE admin (
  admin_id SERIAL PRIMARY KEY,
  email VARCHAR(255),
  hash_password VARCHAR(255),
);

CREATE TABLE job_table (
  jobtitle VARCHAR(255),
  organization VARCHAR(255),
  sta_date DATE,
  end_date DATE,
  position VARCHAR(255),
  emp_number INT,
  employmenttype VARCHAR(255),
  pay DECIMAL(10, 2),
  link VARCHAR(255),
  reqdoc VARCHAR(255),
  person_name VARCHAR(255),
  email VARCHAR(255),
  contact INT,
  tele VARCHAR(255)
);

CREATE TABLE feedback (
  feedback_id SERIAL PRIMARY KEY,
  user_name VARCHAR(30),
  email VARCHAR(255),
  feedback_message VARCHAR(255)
);