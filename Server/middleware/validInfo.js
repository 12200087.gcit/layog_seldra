module.exports = function(req, res, next) {
  const { user_cid, user_name, user_email, user_phone, user_password } = req.body;

  function validEmail(user_mail) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user_mail);
  }

  if (req.path === "/register") {
    console.log(!user_email.length);
    if (![user_cid, user_name, user_email, user_phone, user_password].every(Boolean)) {
      return res.json("Missing Credentials");
    } else if (!validEmail(user_email)) {
      return res.json("Invalid Email");
    }
  } else if (req.path === "/login") {
    if (![user_email,user_password].every(Boolean)) {
      return res.json("Missing Credentials");
    } else if (!validEmail(user_email)) {
      return res.json("Invalid Email");
    }
  }

  next();
};
