const express  = require("express")
const app = express()
const cors=require("cors")
const pool = require("./db")
const session = require('express-session');
//middleware
app.use(cors())
app.use(express.json())
const bcrypt = require('bcrypt');

app.use(session({
  secret: 'your-secret-key',
  resave: false,
  saveUninitialized: true
}));
//create job

// app.use("/authentication", require("./routes/jwtAuth"));

// app.use("/dashboard", require("./routes/dashboard"));

app.post('/admin', (req, res) => {
  const { email, password} = req.body;

  // Hash the password
  bcrypt.hash(password, 10, (err, hash) => {
    if (err) {
      console.error(err);
      res.status(500).json({ message: 'Internal server error' });
    } else {
      // Store the user in the database
      pool.query(
        'INSERT INTO admin (email,hash_password) VALUES ($1, $2)',
        [email, hash,],
        (err, result) => {
          if (err) {
            console.error(err);
            res.status(500).json({ message: 'Internal server error' });
          } else {
            console.log('Admin added');
            res.status(200).json({ message: 'Adminadded' });
          }
        }
      );
    }
  });
});

app.post('/loginAdmin', async (req, res) => {
  const { email, password } = req.body;

  // Check if user with given email exists in the database
  const result = await pool.query('SELECT * FROM admin WHERE email = $1', [email]);
  const user = result.rows[0];

  if (!user) {
    return res.status(401).json({ message: 'Invalid email or password' });
  }

  // Check if the provided password matches the hashed password stored in the database
  const passwordMatches = await bcrypt.compare(password, user.hash_password);

  if (!passwordMatches) {
    return res.status(401).json({ message: 'Invalid email or password' });
  }

  // Set the user session
  req.session.user = {
    email: user.email,
  };

  const username = req.session.user.email;
  console.log(username + " You are in now");

  res.status(200).json({ email: user.email, username });
});


app.post('/signup', (req, res) => {
  const { cid,email,firstName, password,phonenumber } = req.body;

  // Check if the email already exists
  pool.query('SELECT * FROM user1 WHERE cid = $1', [cid], (err, result) => {
    if (err) {
      console.error(err);
      res.status(500).json({ message: 'Internal server error' });
    } else {
      if (result.rows.length > 0) {
        // Email already exists, return an error response
        res.status(400).json({ message: 'user already exists' });
      } else {
        // Hash the password
        bcrypt.hash(password, 10, (err, hash) => {
          if (err) {
            console.error(err);
            res.status(500).json({ message: 'Internal server error' });
          } else {
            // Store the user in the database
            pool.query(
              'INSERT INTO user1 (cid, email,First_name, hash_password,phone_number) VALUES ($1, $2, $3, $4, $5)',
              [cid,email,firstName, hash, phonenumber],
              (err, result) => {
                if (err) {
                  console.error(err);
                  res.status(500).json({ message: 'Internal server error' });
                } else {
                  console.log('User added');
                  res.status(200).json({ message: 'User added' });
                }
              }
            );
          }
        });
      }
    }
  });
});
// app.post('/signup', (req, res) => {
//   const { cid,email,firstName, password,phonenumber} = req.body;

//   // Hash the password
//   bcrypt.hash(password, 10, (err, hash) => {
//     if (err) {
//       console.error(err);
//       res.status(500).json({ message: 'Internal server error' });
//     } else {
//       // Store the user in the database
//       pool.query(
//         'INSERT INTO user1 (cid, email,First_name, hash_password,phone_number) VALUES ($1, $2, $3, $4, $5)',
//         [cid, email, firstName, hash, phonenumber],
//         (err, result) => {
//           if (err) {
//             console.error(err);
//             res.status(500).json({ message: 'Internal server error' });
//           } else {
//             console.log('User added');
//             res.status(200).json({ message: 'User added' });
//           }
//         }
//       );
//     }
//   });
// });



 // Login route
app.post('/login', async (req, res) => {
  const { cid, password } = req.body;

  // Check if user with given email exists in the database
  const result = await pool.query('SELECT * FROM user1 WHERE cid = $1', [cid]);
  const user = result.rows[0];

  if (!user) {
    return res.status(401).json({ message: 'Invalid CID or password' });
  }

  // Check if the provided password matches the hashed password stored in the database
  const passwordMatches = await bcrypt.compare(password, user.hash_password);

  if (!passwordMatches) {
    return res.status(401).json({ message: 'Invalid CID or password' });
  }

  // Set the user session
  req.session.user = {
    cid: user.cid,
    First_name: user.first_name,
  };

  const username = req.session.user.cid;
  console.log(username + " You are in now");

  res.status(200).json({ cid: user.cid, username });
});

 //api to get users
 app.get('/signup',async(req,res)=>{
  try {
      const user = await pool.query('SELECT * FROM user1')
      res.json(user.rows)
  } catch (error) {
      console.error(error)
      
  }
})


app.delete("/signup/:id", async(req, res)=>{
  try{
      const {id} = req.params;
      const {cid,email,firstName, password,phonenumber} = req.body;
      const deleteuser = await pool.query("DELETE FROM user1 WHERE user_id = $1",
      [id]
      );
      res.json("User deleted");
  }
  catch(err){
      console.error(err.message)
  }
})



app.post("/jobs", async(req, res) => {
    try {
        const { jobtitle , organization, sta_date, end_date, position, emp_number, employmenttype, pay, link, reqdoc, person_name, email, contact, tele} = req.body
        // console.log(req.body)
        const newjob=await pool.query(
            "INSERT INTO job_table (jobtitle , organization, sta_date, end_date, position, emp_number, employmenttype, pay, link, reqdoc, person_name, email, contact, tele) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) RETURNING *",
            [jobtitle, organization,sta_date, end_date, position ,emp_number,employmenttype, pay, link,reqdoc,person_name, email,contact ,tele]
        )
        res.json(newjob.rows[0])
        
    } catch (err) {
        console.log(err.message);
        
    }
})

//get all jobs

app.get("/jobs", async (req, res) => {
    try {
      const allJobs = await pool.query("SELECT * FROM job_table ORDER BY job_id DESC");
      res.json(allJobs.rows);
    } catch (err) {
      console.log(err.message);
      res.status(500).send("Server Error");
    }
  });

//get a job

app.get("/jobs/:id", async (req, res) => {
    try {
      const { id } = req.params;
      const job = await pool.query("SELECT * FROM job_table WHERE job_id = $1", [id]);
  
      res.json(job.rows[0]);
    } catch (err) {
      console.log(err.message);
      res.status(500).send("Server Error");
    }
  });



//update a job
app.put('/jobs/:id', async (req, res) => {
    try {
      const { id } = req.params;
      const { jobtitle, organization, sta_date, end_date, position, emp_number, employmenttype, pay, link, reqdoc, person_name, email, contact, tele } = req.body;
  
      const updateJob = await pool.query(
        'UPDATE job_table SET jobtitle=$1, organization=$2, sta_date=$3, end_date=$4, position=$5, emp_number=$6, employmenttype=$7, pay=$8, link=$9, reqdoc=$10, person_name=$11, email=$12, contact=$13, tele=$14 WHERE job_id=$15',
        [jobtitle, organization, sta_date, end_date, position, emp_number, employmenttype, pay, link, reqdoc, person_name, email, contact, tele, id]
      );
  
      res.json('Job was updated');
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ error: 'An error occurred. Please try again.' });
    }
  });

// delete todo

app.delete("/jobs/:id", async(req, res)=>{
    try{
        const {id} = req.params;
        const {jobtitle, organization, sta_date, end_date, position, emp_number, employmenttype, pay, link, reqdoc, person_name, email, contact, tele} = req.body;
        const deletejobs = await pool.query("DELETE FROM job_table WHERE job_id = $1",
        [id]
        );
        res.json("job delete successfully!");
    }
    catch(err){
        console.error(err.message)
    }
})

//feedback

app.post("/feedbacks", async(req, res) => {
  try {
      const { user_name, email, feedback_message} = req.body
      // console.log(req.body)
      const newfeedback=await pool.query(
          "INSERT INTO feedback (user_name, email, feedback_message) VALUES ($1, $2, $3) RETURNING *",
          [user_name, email, feedback_message]
      )
      res.json(newfeedback.rows[0])
      
  } catch (err) {
      console.log(err.message);
      
  }
})

//get all feedback

app.get("/feedbacks", async (req, res) => {
  try {
    const allfeedback = await pool.query("SELECT * FROM feedback ORDER BY feedback_id DESC");
    res.json(allfeedback.rows);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server Error");
  }
});


app.delete("/feedbacks/:id", async(req, res)=>{
  try{
      const {id} = req.params;
      const {user_name, email, feedback_message} = req.body;
      const deletefeedback = await pool.query("DELETE FROM feedback WHERE feedback_id = $1",
      [id]
      );
      res.json("Feedback delete successfully!");
  }
  catch(err){
      console.error(err.message)
  }
})


app.listen(5000, ()=>{
    console.log("Server is running on port 5000")
});